# Nginx as Windown 

Hướng dẫn cài đặt Nginx lên môi trường windown
https://stackoverflow.com/questions/40846356/run-nginx-as-windows-service

- 1.Download NGinx (http://nginx.org/en/download.html) and uzip to C:\foobar\nginx
- 2.Download nssm (https://nssm.cc/)
- 3.Run "nssm install nginx" from the command line
- 4.In NSSM gui do the following:
- 5.On the application tab: 
    set path to C:\foobar\nginx\nginx.exe
    set startup directory to C:\foorbar\nginx
- 6.On the tab I/O :
    Input slow :"start nginx".
    Optionally set C:\foobar\nginx\logs\service.out.log and C:\foobar\nginx\logs\service.err.log in the output and error slots.
- 7.Click "install service". 
    Go to services, start "nginx". 
    Hit http://localhost:80 and you should get the nginx logon. 
    Turn off the service, disable browser cache and refresh, screen should now fail to load.


```
Với trường hợp có sẵn IIS
tải: https://docs.microsoft.com/vi-vn/sysinternals/downloads/tcpview
TcpView
Tắt 2 cái http đi rồi khởi động lại máy.
- Nếu có sql report thì chuyển port 80 đi.
```